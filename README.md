# TechnicalPaper

## Discuss any 5 design patterns used in JavaScript with code samples.
You can run all the patterns at once using index.js.
___
### Pattern 1
Code is in the patten1.js
```sh
Input 4
Output
         *
      *  *
   *  *  *
*  *  *  *
```
So in pattern 1, we have to create the half side of the pyramid so its height and length are the same n. Here you can see a pattern is repeating the spaces are reducing and stars are increasing. so we create a loop in which step is set to 1 which will go up to n. Then we have to create a variable that stores the value of a single line and prints it at the end of the loop. now we have to add n - i spaces and i '*' to string and print it. 

___
### Pattern 2
Code is in the patten2.js
```sh
Input 4
Output
*                   *
* * *           * * *
* * * * *   * * * * *
* * * * * * * * * * *
```
In pattern 2 we have to create the above pattern. Its height is n. so we create a loop in which step is set to 1 which will go up to n. Then we have to create a variable that stores the value of a single line and prints it at the end of the loop. we add 2 * i-1 * at the start of the string then add 4 * (n - i)-3 spaces in the middle and again 2 * -i-1 * in the end but when i is equal to n then we add 2 * i -3 * in the end.
___
### Pattern 3
Code is in the patten3.js
```sh
Input 4
Output
         *
           * *
             * * *
               * * * *
             * * *
           * *
         *
```
In pattern 3 we have to create the above pattern you can see 2 patterns first in which * increases and in Second * decreases. Its height is 2*n-1.
   1. First Pattern:- So we create a loop in which step is set to 1 which will go up to n. Then we have to create a variable that stores the value of a single line and prints it at the end of the loop. We add 2*i -1 and n - i spaces in the start then add i * to the sting.
   2. Second Pattern:-  So we create a loop in which step is set to n-1 which will go up to 1. Then we have to create a variable that stores the value of a single line and prints it at the end of the loop. We add 2*i -1 and n - i spaces in the start then add i * to the sting.
___
### Pattern 4
Code is in the patten4.js
```sh
Input 5
Output
*                         *
   *                   *
*     *             *     *
   *     *       *     *
*     *      *      *     *
   *     *       *     *
*     *             *     *
   *                   *
*                         *
```
In pattern 4 we have to create the above pattern you can see 2 patterns first in which * increases and in Second * decreases. Its height is 2*n-1.
In this pattern the special thing is the * is printed if i and j are both even or odd.
___
### Pattern 5
Code is in the patten5.js
```sh
Input 5
Output
*   *   *   *   *
  *   *   *   *
    *   *   *
      *   *
        *
      *   *
    *   *   *
  *   *   *   *
*   *   *   *   *
```
In pattern 4 we have to create above pattern you can see 2 patterns first in which * increases and in Second * decreases. Its height is 2*n-1.
In this pattern, the special thing is the * is printed if i and j are both even or odd. 
   1. First Pattern:- So we create a loop in which step is set to 1 which will go up to n. Then we have to create a variable that stores the value of a single line and prints it at the end of the loop. We add n-i spaces in the start then i * and spaces.
   2. Second Pattern:-  So we create a loop in which step is set to 2 which will go up to n. Then we have to create a variable that stores the value of a single line and prints it at the end of the loop. We add n-i spaces in the start then i * and spaces.