/*
input 5
output
*                         *
   *                   *
*     *             *     *
   *     *       *     *
*     *      *      *     *
   *     *       *     *
*     *             *     *
   *                   *
*                         *
*/
function patten4(lines){
    for(let i = 1;i < lines + 1;i++ ){
        s = ''
        for(let j = 0;j < i;j++ ){
            if((i%2 != 0 && j%2 != 0)||(i%2 == 0 && j%2 == 0)){
                s += '  '
            } else {
                s += '* '
            }
        }
        for(let j = 0;j < 2*(lines - i) -1 ;j++ ){
            s += '  '
        }  
        if(i === lines){
            for(let j = 0;j < i-1;j++ ){
                if(lines%2 == 0){
                    if((i%2 != 0 && j%2 != 0)||(i%2 == 0 && j%2 == 0)){
                        s += '  '
                    } else {
                        s += '* '
                    }                
                } else{
                    if((i%2 == 0 && j%2 != 0)||(i%2 != 0 && j%2 == 0)){
                        s += '  '
                    } else {
                        s += '* '
                    }    
                }
            }
        }else{
            for(let j = 0;j < i;j++ ){
                if(lines%2 == 0){ 
                    if(i%2 != 0){
                        if((i%2 != 0 && j%2 == 0)||(i%2 != 0 && j%2 == 0)){
                            s += '* '
                        } else {
                            s += '  '
                        }                
                    } else{
                        if((i%2 == 0 && j%2 != 0)||(i%2 != 0 && j%2 == 0)){
                            s += '  '
                        } else {
                            s += '* '
                        }    
                    }
                } else{
                    if(i%2 != 0){
                        if((i%2 != 0 && j%2 == 0)||(i%2 != 0 && j%2 == 0)){
                            s += '* '
                        } else {
                            s += '  '
                        }                
                    } else{
                        if((i%2 == 0 && j%2 != 0)||(i%2 != 0 && j%2 == 0)){
                            s += '  '
                        } else {
                            s += '* '
                        }
                    }
                }
            } 
        } 
        console.log(s)
    }
    for(let i = lines-1;i > 0 ;i-- ){
        s = ''
        for(let j = 0;j < i;j++ ){
            if((i%2 != 0 && j%2 != 0)||(i%2 == 0 && j%2 == 0)){
                s += '  '
            } else {
                s += '* '
            }
        }
        for(let j = 0;j < 2*(lines - i)-1 ;j++ ){
            s += '  '
        }  
        
        for(let j = 0;j < i;j++ ){
            if(lines%2 == 0){ 
                if(i%2 != 0){
                    if((i%2 != 0 && j%2 == 0)||(i%2 != 0 && j%2 == 0)){
                        s += '* '
                    } else {
                        s += '  '
                    }                
                } else{
                    if((i%2 == 0 && j%2 != 0)||(i%2 != 0 && j%2 == 0)){
                        s += '  '
                    } else {
                        s += '* '
                    }    
                }
            }else{
                if(i%2 != 0){
                    if((i%2 != 0 && j%2 == 0)||(i%2 != 0 && j%2 == 0)){
                        s += '* '
                    } else {
                        s += '  '
                    }                
                } else{
                    if((i%2 == 0 && j%2 != 0)||(i%2 != 0 && j%2 == 0)){
                        s += '  '
                    } else {
                        s += '* '
                    }
                }
            }
        }     
        console.log(s)
    }
}
module.exports = patten4;